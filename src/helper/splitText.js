const splitText = (booksList) => {

  // console.log('split')
  // this is an array holding a split up message
  // so that I can actually send the message per text
  const splitMessage = []
  let j = 0
  let i = 0
  let k = 70
  while (booksList[k]) {
    splitMessage[j] = booksList.substr(i, k)
    j++
    i += 70
    k += 70
  }
  return splitMessage
}

module.exports = splitText
