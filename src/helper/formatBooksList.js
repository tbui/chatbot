// This is here to make the bookObject into a printable string
const formatBooksList = (booksObject) => {
  let booksList = '\n'
  for (let i = 0; booksObject[i]; i++) {
    booksList = booksList + booksObject[i] + ', '
  }
  return booksList
}

module.exports = formatBooksList
