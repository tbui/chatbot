const config = {
  recast: {
    token: '27e37d29bcc2d7a29563b5d402201de3',
    language: 'en',
  },
  grs: {
    authorInfoURL: 'http://isbndb.com/api/v2/json/Q78EQD3W/author/',
    bookInfoURL: 'http://isbndb.com/api/v2/json/Q78EQD3W/book/',
    key: 'Q78EQD3W',
    secret:'0i1l0DJuUBhBc4wAsReUe7G332mfFxv1fbBzVy2d5GA',
  },
  port: 8080,
}

module.exports = config
