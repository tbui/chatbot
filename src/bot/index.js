
const callr = require('callr');
const request = require('superagent')
const express = require('express')
const bodyParser = require('body-parser')
import * as recast from 'recastai'

const findBooks = require('../external/findBook.js')
const BookInfo = require('../external/getBookInfo.js')
const formatBooksList = require('../helper/formatBooksList.js')
const splitText = require('../helper/splitText.js')
const config = require('../config.js')

// initiating my express app below
// initiating the API with my credentials below
// making the app listen on port number (to be found in the config file)
const app = express()
const api = new callr.api('recastai_3', '7uoKNQ4q5k')
const server = app.listen(config.port)
const recastClient = new recast.Client(config.recast.token, config.recast.language)

// Defintion of my method, endpoint, and unnecessary option


// for parsing application/json
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

// next I need to get my bot hooked up and then get the SMS into its memory



const type = 'sms.mo'
const endpoint = 'https://050d6283.ngrok.io'
const options = null

// I suppose this is my subscription to the webhook of Callr
api.call('webhooks.subscribe', type, endpoint, options)
  .success(function(response) {
    console.log('Subscription to webhook executed')
  })

//api.call('sms.send', 'CALLR', '+33768048917', 'Test' , null) //testing

app.post('/', function (req, res) {
  res.send('POST request to homepage')
  recastClient.textConverse(req.body.data.text, { conversationToken: req.body.data.from })
  .then((res) => {
    const replies = res.replies
    const action = res.action

    console.log(action.slug) // testing
    console.log(action.done) // testing

    if (!replies.length) {
      api.call('sms.send', req.body.data.to, req.body.data.from, 'Sorry1' , null)
      return
    }
    if (action && action.done) {
      handleAction(action, res, req)
    }
    replies.forEach(reply => api.call('sms.send', req.body.data.to, req.body.data.from, reply , null))
  })
  .catch(err => api.call('sms.send', req.body.data.to, req.body.data.from , 'Sorry2' , null))
})


const handleAction = (action, res, req) => {
    // so if the user agrees that it's indeed the right writer
    if (action.slug === 'agree-1' && action.done) {
      // putting the memory in here to extract the writer's name
      findBooks(res.memory)
          .then((booksObject) => {
            //console.log('here?')
            const booksList = formatBooksList(booksObject)
            const splitTextMessage = splitText(booksList)
            splitTextMessage.forEach(function(books) {
              api.call('sms.send', req.body.data.to, req.body.data.from, books, null)
                .error((err) => {
                 api.call('sms.send', req.body.data.to, req.body.data.from, 'Sorry3' , null)
               })
            })
          })
          .catch((err) => {
            console.log(err)
           api.call('sms.send', req.body.data.to, req.body.data.from, 'Sorry4' , null)
          })
    } else if (action.slug === 'whichbook') {
      // here we fire back the info of whatever book was chosen
      BookInfo(res.memory)
          .then(bookinfos => {
          api.call('sms.send', req.body.data.to, req.body.data.from, bookinfos , null)
            .error(() => api.call('sms.send', req.body.data.to, req.body.data.from, 'Sorry5' , null))
          })
          .catch(() => api.call('sms.send', req.body.data.to, req.body.data.from, 'Sorry6' , null))
    }
}
